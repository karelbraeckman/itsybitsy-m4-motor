/*
  DistanceDriver
*/

#include "Arduino.h"
#include "DistanceDriver.h"

DistanceDriver::DistanceDriver(PidMotor *theMotor, long pulsesPerRev, float wheelCircumferenceMillimeter) {
  motor = theMotor;
  setup(pulsesPerRev, wheelCircumferenceMillimeter);
  reset();
}

void DistanceDriver::reset() {
  ticksDriven = 0;
  stopAtTicks = -1;
  motor->stop();
  _direction = INACTIVE;
}

void DistanceDriver::setup(long pulsesPerRev, float wheelCircumferenceMillimeter) {
  _pulsesPerRev = pulsesPerRev;
  _wheelCircumferenceMillimeter = wheelCircumferenceMillimeter;
}

void DistanceDriver::driveTicks(long ticks) {
  ticksDriven = 0;
  stopAtTicks = ticks;
  if (ticks > 0) {
    motor->setSpeed(100L);
    _direction = FORWARD;
  } else {
    motor->setSpeed(-100L);
    _direction = BACKWARD;
  }
}

void DistanceDriver::driveMillimeters(long millimeters) {
  long ticks = (millimeters * _pulsesPerRev) / _wheelCircumferenceMillimeter;
  driveTicks(ticks);
}

void DistanceDriver::gotTicks(long ticks) {
  ticksDriven += ticks;
  if (
    ((_direction == FORWARD) && (ticksDriven >= stopAtTicks))
    || ((_direction == BACKWARD) && (ticksDriven <= stopAtTicks))
  ) {
    reset();
  }
}

bool DistanceDriver::isBusy() {
  if (_direction == INACTIVE) {
    return false;
  }
  return true;
}
