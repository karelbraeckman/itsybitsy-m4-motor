/*
  PidMotor.h - Library for controlling motor speed with a PID
  Created by Karel Braeckman, sep 2020
  Do whatever you want with it
*/

#include "Arduino.h"
#include "PidMotor.h"


// PID-Controller for wheel speed
// - input is the wheel speed in mm/sec (mm the wheel travels when rolling on the ground per second)
// - output is the PWM value
// - setpoint is the desired wheel speed in mm/sec
//
// The tuning of Kp, Ki, Kd was done based on https://en.wikipedia.org/wiki/Ziegler%E2%80%93Nichols_method#cite_note-microstar-2

PidMotor::PidMotor(int pin1, int pin2) : motor(pin1, pin2), pid(&pidInput, &pidOutput, &pidSetpoint, 1.44, 12.3, 0.01, DIRECT) {
  pidSetpoint=0;
  pidInput=0;
  pid.SetMode(MANUAL);
  pid.SetOutputLimits(-255, 255);
  pid.SetSampleTime(40);
}

void PidMotor::setPWM(int pwm1, int pwm2) {
  motor.setPWM(pwm1, pwm2);
}

void PidMotor::setPWMSpeed(int pwmSpeed) {
  motor.setPWMSpeed(pwmSpeed);
}

double PidMotor::updatePid(double actualSpeed) {
  if (pid.GetMode() == AUTOMATIC) {
    pidInput = actualSpeed;
    pid.Compute();
    motor.setPWMSpeed((int)pidOutput);
    return pidOutput;
  }
  return 0;
}

void PidMotor::stop() {
  pid.SetMode(MANUAL);
  motor.setPWM(0, 0);
}

void PidMotor::setTunings(double Kp, double Ki, double Kd) {
  pid.SetTunings(Kp, Ki, Kd);
}

void PidMotor::setSpeed(long mmPerSec) {
  pidSetpoint = (double)mmPerSec;
  pid.SetMode(AUTOMATIC);
}

void PidMotor::plot() {
    // Serial Plotter
  if (pid.GetMode() == AUTOMATIC) {
    Serial.println("speed pwm setpoint");
    Serial.print(pidInput, DEC);
    Serial.print(" ");
    Serial.print(pidOutput);
    Serial.print(" ");
    Serial.print(pidSetpoint);
    Serial.print("\r\n");
  }
}
