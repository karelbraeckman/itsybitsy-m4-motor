/*
	EncoderSender.h - Library which will send encoder data at most N times per second over serial
*/

#include "Arduino.h"
#include <Encoder.h>
#include "EncoderSender.h"
#include <math.h>

EncoderSender::EncoderSender(Encoder *encoderLeft, Encoder *encoderRight, Encoder *encoderX, Encoder *encoderY, long pulsesPerRev, float wheelCircumferenceMillimeter, int leftRightWheelDistanceMillimeters, unsigned int throttleMs, unsigned int serialThrottleMs) {
	_encoderLeft = encoderLeft;
	_encoderRight = encoderRight;
  _encoderX = encoderX;
  _encoderY = encoderY;
	_throttleMs = throttleMs;
  _serialThrottleMs = serialThrottleMs;
	_lastFlushTime = -1;
  _lastSerialSendTime = -1;
  _totalTicksLeft = 0;
  _totalTicksRight = 0;
  _totalTicksX = 0;
  _totalTicksY = 0;
  setup(pulsesPerRev, wheelCircumferenceMillimeter, leftRightWheelDistanceMillimeters);
  encoderUpdate.period = -1;
  encoderUpdate.ticksLeft = 0;
  encoderUpdate.ticksRight = 0;
  encoderUpdate.ticksX = 0;
  encoderUpdate.ticksY = 0;
  encoderUpdate.tickSpeedLeft = 0;
  encoderUpdate.tickSpeedRight = 0;
  encoderUpdate.tickSpeedX = 0;
  encoderUpdate.tickSpeedY = 0;
  encoderUpdate.mmSpeedLeft = 0;
  encoderUpdate.mmSpeedRight = 0;
  encoderUpdate.mmSpeedX = 0;
  encoderUpdate.mmSpeedY = 0;

  // initial position
  _umX = 0;
  _umY = 0;
  _deciDegreesPhi = 0;
}

void EncoderSender::setCommunicator(Communicator *theCommunicator) {
  communicator = theCommunicator;
}

void EncoderSender::setup(long pulsesPerRev, float wheelCircumferenceMillimeter, int leftRightWheelDistanceMillimeters) {
  _pulsesPerRev = pulsesPerRev;
  _wheelCircumferenceMillimeter = wheelCircumferenceMillimeter;
  _leftRightWheelDistanceMillimeters = leftRightWheelDistanceMillimeters;
}

void EncoderSender::setThrottle(int throttleMs, int serialThrottleMs) {
  _throttleMs = throttleMs;
  _serialThrottleMs = serialThrottleMs;
}

Pose* EncoderSender::getPose() {
  _pose.mmX = _umX / 1000;
  _pose.mmY = _umY / 1000;
  _pose.degreesPhi = _deciDegreesPhi / 10;
  while (_pose.degreesPhi < -180) {
    _pose.degreesPhi += 360;
  }
  while (_pose.degreesPhi > 180) {
    _pose.degreesPhi -= 360;
  }  
  return &_pose;
}

/**
 * If a pose estimate is known, call setPose to set internal state
 * to it
 * @param pose  The Pose update
 **/ 
void EncoderSender::setPose(Pose *pose) {
  _umX = pose->mmX * 1000;
  _umY = pose->mmY * 1000;
  _deciDegreesPhi = pose->degreesPhi * 10;
}

/**
 * Should be called often to update state. It will respect throttleMs, so call it as often as you want.
 **/ 
EncoderUpdate* EncoderSender::update() {
	unsigned long _now = millis();
	unsigned long _millis_passed = (_now - _lastFlushTime);
  encoderUpdate.period = -1;

  // respect throttleMs
  if (_millis_passed < _throttleMs) {
    return &encoderUpdate;
  }

  // read encoder ticks and update totals
  if (_encoderLeft != NULL) {
    encoderUpdate.ticksLeft = _encoderLeft->read();
  } else {
    encoderUpdate.ticksLeft = 0;
  }
  if (_encoderRight != NULL) {
    encoderUpdate.ticksRight = _encoderRight->read();
  } else {
    encoderUpdate.ticksRight = 0;
  }
  if (_encoderX != NULL) {
    encoderUpdate.ticksX = _encoderX->read();
  } else {
    encoderUpdate.ticksX = 0;
  }
  if (_encoderY != NULL) {
    encoderUpdate.ticksY = _encoderY->read();
  } else {
    encoderUpdate.ticksY = 0;
  }
  _totalTicksLeft += encoderUpdate.ticksLeft;
  _totalTicksRight += encoderUpdate.ticksRight;
  _totalTicksX += encoderUpdate.ticksX;
  _totalTicksY += encoderUpdate.ticksY;
  encoderUpdate.period = _millis_passed;
  encoderUpdate.tickSpeedLeft = (1000L * encoderUpdate.ticksLeft) / ((long)_millis_passed);
  encoderUpdate.tickSpeedRight = (1000L * encoderUpdate.ticksRight) / ((long)_millis_passed);
  encoderUpdate.tickSpeedX = (1000L * encoderUpdate.ticksX) / ((long)_millis_passed);
  encoderUpdate.tickSpeedY = (1000L * encoderUpdate.ticksY) / ((long)_millis_passed);
  encoderUpdate.mmSpeedLeft = (encoderUpdate.tickSpeedLeft * _wheelCircumferenceMillimeter) / _pulsesPerRev;
  encoderUpdate.mmSpeedRight = (encoderUpdate.tickSpeedRight * _wheelCircumferenceMillimeter) / _pulsesPerRev;
  encoderUpdate.mmSpeedX = (encoderUpdate.tickSpeedX * _wheelCircumferenceMillimeter) / _pulsesPerRev;
  encoderUpdate.mmSpeedY = (encoderUpdate.tickSpeedY * _wheelCircumferenceMillimeter) / _pulsesPerRev;

  // update pose
  encoderUpdate.umLeft = (encoderUpdate.ticksLeft * _wheelCircumferenceMillimeter * 1000.0) / _pulsesPerRev;
  encoderUpdate.umRight = (encoderUpdate.ticksRight * _wheelCircumferenceMillimeter * 1000.0) / _pulsesPerRev;
  encoderUpdate.umX = (encoderUpdate.ticksX * _wheelCircumferenceMillimeter * 1000.0) / _pulsesPerRev;
  encoderUpdate.umY = (encoderUpdate.ticksY * _wheelCircumferenceMillimeter * 1000.0) / _pulsesPerRev;

  long umCenter = (encoderUpdate.umLeft + encoderUpdate.umRight) / 2;
  _deciDegreesPhi += 1800.0 * (((float)encoderUpdate.umRight - (float)encoderUpdate.umLeft) / (1000.0 * (float)_leftRightWheelDistanceMillimeters)) / PI;
  while (_deciDegreesPhi < -1800) {
    _deciDegreesPhi += 3600;
  }
  while (_deciDegreesPhi > 1800) {
    _deciDegreesPhi -= 3600;
  }
  _umX += umCenter * cos(((float)_deciDegreesPhi) * PI / 1800.0);
  _umY += umCenter * sin(((float)_deciDegreesPhi) * PI / 1800.0);
  
  _lastFlushTime = _now;
  if (_encoderLeft != NULL) {
    _encoderLeft->write(0);
  }
  if (_encoderRight != NULL) {
    _encoderRight->write(0);
  }
  if (_encoderX != NULL) {
    _encoderX->write(0);
  }
  if (_encoderY != NULL) {
    _encoderY->write(0);
  }
  if (_now - _lastSerialSendTime >= _serialThrottleMs) {
    _mmLeft = (_totalTicksLeft * _wheelCircumferenceMillimeter) / _pulsesPerRev;
    _mmRight = (_totalTicksRight * _wheelCircumferenceMillimeter) / _pulsesPerRev;
    _mmX = (_totalTicksX * _wheelCircumferenceMillimeter) / _pulsesPerRev;
    _mmY = (_totalTicksY * _wheelCircumferenceMillimeter) / _pulsesPerRev;

    _umLeft = (1000 * _totalTicksLeft * _wheelCircumferenceMillimeter) / _pulsesPerRev;
    _umRight = (1000 * _totalTicksRight * _wheelCircumferenceMillimeter) / _pulsesPerRev;
    _umXMotor = (1000 * _totalTicksX * _wheelCircumferenceMillimeter) / _pulsesPerRev;
    _umYMotor = (1000 * _totalTicksY * _wheelCircumferenceMillimeter) / _pulsesPerRev;

    if (_umLeft != 0 || _umRight != 0 || _umXMotor != 0 || _umYMotor != 0) {
      // write out wheel distances
      _encoderSendStr = "<DIST,";
      _encoderSendStr += _mmLeft;
      _encoderSendStr += ",";
      _encoderSendStr += _mmRight;
      _encoderSendStr += ",";
      _encoderSendStr += _mmX;
      _encoderSendStr += ",";
      _encoderSendStr += _mmY;
      _encoderSendStr += ">";
      communicator->println(_encoderSendStr);

      // write out wheel distances in micrometer, will replace DIST in the future
      _encoderSendStr = "<DISTUM,";
      _encoderSendStr += _umLeft;
      _encoderSendStr += ",";
      _encoderSendStr += _umRight;
      _encoderSendStr += ",";
      _encoderSendStr += _umXMotor;
      _encoderSendStr += ",";
      _encoderSendStr += _umYMotor;
      _encoderSendStr += ">";
      communicator->println(_encoderSendStr);
      
      // write out pose
      int _tmpMmX = (int)round(_umX / 1000.0);
      int _tmpMmY = (int)round(_umY / 1000.0);
      int _tmpDegreesPhi = (int)round(_deciDegreesPhi / 10.0);
      _encoderSendStr = "<POSE,";
      _encoderSendStr += _tmpMmX;
      _encoderSendStr += ",";
      _encoderSendStr += _tmpMmY;
      _encoderSendStr += ",";
      _encoderSendStr += _tmpDegreesPhi;
      _encoderSendStr += ">";
      communicator->println(_encoderSendStr);
    }
    _lastSerialSendTime = _now;
    _totalTicksLeft = 0;
    _totalTicksRight = 0;
    _totalTicksX = 0;
    _totalTicksY = 0;
  }
  return &encoderUpdate;
}
