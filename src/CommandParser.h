/*
	CommandParser.h - Library which reads serial data without blocking and parses commands
	Code almost entirely taken from https://forum.arduino.cc/index.php?topic=396450.0
*/

#ifndef CommandParser_h
#define CommandParser_h
#include "Arduino.h"
#include "Communicator.h"
#define BUFFER_CHARS 32

struct Command {
  int id;
  char name[BUFFER_CHARS];
  int param1;
  int param2;
  int param3;
  int param4;
  int param5;
  int param6;
  int param7;
  int param8;
  boolean isValid;
};

class CommandParser {
	public:
		CommandParser();
		void recvWithStartEndMarkers();
		void parseData();
		Command update();
		void setCommunicator(Communicator *theCommunicator);
		Communicator *communicator;
	private:
		char receivedChars[BUFFER_CHARS];
		char tempChars[BUFFER_CHARS];

		// variables to hold the parsed data
    Command lastCommand = { 0, {0}, 0, 0, 0, 0, 0, 0, 0, 0, false };

		boolean newData = false;
};

#endif
