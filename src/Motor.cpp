/*
	Motor.h - Library for driving a motor using two PWM pins
	Created by Karel Braeckman, sep 2020
	Do whatever you want with it
*/

#include "Arduino.h"
#include "Motor.h"

Motor::Motor(int pin1, int pin2) {
	pinMode(pin1, OUTPUT);
	pinMode(pin2, OUTPUT);
	_pin1 = pin1;
	_pin2 = pin2;
	digitalWrite(pin1, LOW);
	digitalWrite(pin2, LOW);
}

void Motor::setPWM(int pwm1, int pwm2) {
  // https://learn.adafruit.com/introducing-itsy-bitsy-m0/adapting-sketches-to-m0#analogwrite-pwm-range-2677096-7
  analogWrite(_pin1, pwm1);
	analogWrite(_pin2, pwm2);
}

void Motor::setPWMSpeed(int pwmSpeed) {
  if (pwmSpeed >= 0) {
    setPWM(0, pwmSpeed);
  } else {
    setPWM(abs(pwmSpeed), 0);
  }
}

void Motor::forward() {
  setPWM(255, 0);
}

void Motor::backward() {
	setPWM(0, 255);
}
