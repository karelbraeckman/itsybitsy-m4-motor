#include "Arduino.h"
#include "SerialCommunicator.h"


SerialCommunicator::SerialCommunicator(Stream *streamObject) {
    setSerial(streamObject);
}

SerialCommunicator::SerialCommunicator() {
    // use Serial1 by default
    setSerial(&Serial1);
}

void SerialCommunicator::setSerial(Stream *streamObject) {
  commandSerial = streamObject;
}

void SerialCommunicator::print(const char c[]) {
    commandSerial->print(c);
}

void SerialCommunicator::println(const char c[]) {
    commandSerial->println(c);
}

void SerialCommunicator::print(String c) {
    commandSerial->print(c);
}

void SerialCommunicator::println(String c) {
    commandSerial->println(c);
}

int SerialCommunicator::available(void) {
    return commandSerial->available();
}

int SerialCommunicator::read(void) {
    return commandSerial->read();
}