/*
	EncoderSender.h - Library which will send encoder data at most N times per second over serial
*/

#ifndef EncoderSender_h
#define EncoderSender_h
#include "Arduino.h"
#include "Communicator.h"
#include "SerialCommunicator.h"
/** Used to describe motor updates during a short amount of time */
struct EncoderUpdate {
  long period;          // amount of milliseconds this update is about
  long ticksLeft;       // ticks left motor had during period
  long ticksRight;      // ticks right motor had during period
  long ticksX;          // ticks x motor had during period
  long ticksY;          // ticks y motor had during period
  long tickSpeedLeft;   // left motor speed in ticks per second
  long tickSpeedRight;  // right motor speed in ticks per second
  long tickSpeedX;      // x motor speed in ticks per second
  long tickSpeedY;      // y motor speed in ticks per second
  long umLeft;          // micrometers left motor has driven during period
  long umRight;         // micrometers right motor has driven during period
  long umX;             // micrometers x motor has driven during period
  long umY;             // micrometers y motor has driven during period
  long mmSpeedLeft;     // left motor speed in millimeters per second
  long mmSpeedRight;    // right motor speed in millimeters per second
  long mmSpeedX;        // x motor speed in millimeters per second
  long mmSpeedY;        // y motor speed in millimeters per second
};

struct Pose {
  long mmX;
  long mmY;
  long degreesPhi;
};

class EncoderSender {
	public:
		EncoderSender(Encoder *encoderLeft, Encoder *encoderRight, Encoder *encoderX, Encoder *encoderY, long pulsesPerRev, float wheelCircumferenceMillimeter, int leftRightWheelDistanceMillimeters, unsigned int updateThrottleMs, unsigned int serialUpdateThrottleMs);
    void setup(long pulsesPerRev, float wheelCircumferenceMillimeter, int leftRightWheelDistanceMillimeters);
		EncoderUpdate* update();
    Pose* getPose();
    void setPose(Pose *pose);
    void setThrottle(int throttleMs, int serialThrottleMs);
    void setCommunicator(Communicator *communicator);
    Communicator *communicator;
	private:
		Encoder *_encoderLeft;
		Encoder *_encoderRight;
    Encoder *_encoderX;
    Encoder *_encoderY;
		unsigned int _throttleMs;
    unsigned int _serialThrottleMs;
		unsigned long _lastFlushTime;
    unsigned long _lastSerialSendTime;
    long _leftSpeedTicksPerSecond;
    long _rightSpeedTicksPerSecond;
    long _xSpeedTicksPerSecond;
    long _ySpeedTicksPerSecond;
    long _totalTicksLeft; // ticks left motor since last serial write
    long _totalTicksRight; // ticks right motor since last serial write
    long _totalTicksX; // ticks x motor since last serial write
    long _totalTicksY; // ticks y motor since last serial write
    long _mmLeft; // mm left motor since last serial write
    long _mmRight; // mm right motor since last serial write
    long _mmX; // mm x motor since last serial write
    long _mmY; // mm y motor since last serial write
    long _umLeft; // micrometer left motor since last serial write, will replace _mmLeft in the future
    long _umRight; // micrometer right motor since last serial write
    long _umXMotor; // micrometer x motor since last serial write
    long _umYMotor; // micrometer y motor since last serial write
    String _encoderSendStr;
    long _pulsesPerRev;
    float _wheelCircumferenceMillimeter;
    int _leftRightWheelDistanceMillimeters;
    EncoderUpdate encoderUpdate; // this gets updated once per throttleMs if update() is called
    long _umX; // estimated x position in micrometers
    long _umY; // estimated y position in micrometers
    long _deciDegreesPhi; // estimated heading in tenth of degrees

    Pose _pose;
};

#endif
