/*
  DistanceDriver.h
*/

#ifndef Distance_Driver_h
#define Distance_Driver_h
#include "Arduino.h"
#include "PidMotor.h"

enum DistanceDriverDirection {
  FORWARD,
  BACKWARD,
  INACTIVE
};

class DistanceDriver {
  public:
    DistanceDriver(PidMotor *theMotor, long pulsesPerRev, float wheelCircumferenceMillimeter);
    void driveTicks(long ticks);
    void driveMillimeters(long millimeters);
    void gotTicks(long ticks);
    void reset();
    void setup(long pulsesPerRev, float wheelCircumferenceMillimeter);
    bool isBusy();
  private:
    PidMotor *motor;
    long ticksDriven;
    long stopAtTicks;
    long _pulsesPerRev;
    float _wheelCircumferenceMillimeter;
    DistanceDriverDirection _direction;
};

#endif
