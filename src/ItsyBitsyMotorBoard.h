#ifndef ItsyBitsyMotorBoard_h
#define ItsyBitsyMotorBoard_h
#include "Arduino.h"
#include "MotorBoardSettings.h"
#include "Communicator.h"
#include "PidMotor.h"
#include <Encoder.h>
#include "DistanceDriver.h"
#include "EncoderSender.h"
#include "CommandParser.h"
#include "Kinematics.h" //https://github.com/linorobot/kinematics
#include "SerialCommunicator.h"

#ifndef PI
#define PI (3.141592)
#endif

#define MOTOR_MAX_RPM 60        // motor's maximum rpm
#define PWM_BITS 8              // microcontroller's PWM pin resolution

class ItsyBitsyMotorBoard {
    public:
        ItsyBitsyMotorBoard(void);
        void setup(long pulsesPerRev, int wheelDiameterMillimeters, int leftRightWheelDistanceMillimeters);
        void loop(void);
        void resolveCommandId(int commandId);
        void rejectCommandId(int commandId, String message);
        void waitForCommand(int commandId, String commandName);
        void setSpeed2(long vLinear, long omega);
        bool handleCommand(Command *theCommand);
        //void setCommandSerial(Stream *streamObject);
        void setCommunicator(Communicator *communicator);
        // command parser will read data sent through Serial1 in a non-blocking fashion and
        // parses commands
        CommandParser commandParser;
        PidMotor *motorLeft;
        PidMotor *motorRight;
        PidMotor *motorX;
        PidMotor *motorY;
        Encoder *encoderLeft;
        Encoder *encoderX;
        Encoder *encoderRight;
        Encoder *encoderY;
        EncoderUpdate *encoderUpdate;
        Pose *currentPose;
        EncoderSender *encoderSender;
        int mmSpeedLeft;
        int mmSpeedRight;
        int mmSpeedX;
        int mmSpeedY;
        //Stream *commandSerial;
        Communicator *communicator;
        MotorBoardSettings settings;
    private:
        long _pulsesPerRev;
        int _wheelDiameterMillimeters;
        int _leftRightWheelDistanceMillimeters;
        float _wheelCircumferenceMillimeter;
        float _robotCircumferenceMillimeter;
        DistanceDriver *distanceDriverLeft;
        DistanceDriver *distanceDriverRight;
        DistanceDriver *distanceDriverX;
        DistanceDriver *distanceDriverY;
        Kinematics *kinematics;
        String waitForCommandName;
        int waitForCommandId;
        long resolveWaitCommandOnMillis;
        long millisNow;
        bool goToGoalEnabled;
        Pose goal;
};

#endif