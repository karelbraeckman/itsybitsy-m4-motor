/*
	CommandParser.h - Library which reads serial data without blocking and parses commands
	Code almost entirely taken from https://forum.arduino.cc/index.php?topic=396450.0
*/

#include "Arduino.h"
#include "CommandParser.h"


CommandParser::CommandParser()
{
	//setInterface(&Serial1);
}

void CommandParser::setCommunicator(Communicator *theCommunicator) {
	communicator = theCommunicator;
}

void CommandParser::recvWithStartEndMarkers() {
	static boolean recvInProgress = false;
	static byte ndx = 0;
	char startMarker = '<';
	char endMarker = '>';
	char rc;

	while (communicator->available() > 0 && newData == false) {
		rc = communicator->read();

		if (recvInProgress == true) {
			if (rc != endMarker) {
				receivedChars[ndx] = rc;
				ndx++;
				if (ndx >= BUFFER_CHARS) {
					ndx = BUFFER_CHARS - 1;
				}
			}
			else {
				receivedChars[ndx] = '\0'; // terminate the string
				recvInProgress = false;
				ndx = 0;
				newData = true;
			}
		}

		else if (rc == startMarker) {
			recvInProgress = true;
		}
	}
}

void CommandParser::parseData() {
	char * strtokIndx; // this is used by strtok() as an index
  	lastCommand.id = 0;
	lastCommand.param1 = 0;
	lastCommand.param2 = 0;
	lastCommand.param3 = 0;
	lastCommand.param4 = 0;
	lastCommand.param5 = 0;
	lastCommand.param6 = 0;
	lastCommand.param7 = 0;
	lastCommand.param8 = 0;

	strtokIndx = strtok(tempChars,",");      // get the first part - the id
	if (strtokIndx == NULL) {
		return;
	}
	lastCommand.id = atoi(strtokIndx);     // convert this part to an integer

	strtokIndx = strtok(NULL, ",");      // get the second part - the command string
	strcpy(lastCommand.name, strtokIndx); // copy it to messageFromPC
  	lastCommand.isValid = true;

	strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
	if (strtokIndx == NULL) {
	  return;
	}
	lastCommand.param1 = atoi(strtokIndx);     // convert this part to an integer

	strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
	  return;
	}
	lastCommand.param2 = atoi(strtokIndx);     // convert this part to an integer

	strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
	  return;
	}
	lastCommand.param3 = atoi(strtokIndx);     // convert this part to an integer

	strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
		return;
	}
	lastCommand.param4 = atoi(strtokIndx);     // convert this part to an integer

    strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
		return;
	}
	lastCommand.param5 = atoi(strtokIndx);     // convert this part to an integer

    strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
		return;
	}
	lastCommand.param6 = atoi(strtokIndx);     // convert this part to an integer

    strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
		return;
	}
	lastCommand.param7 = atoi(strtokIndx);     // convert this part to an integer

    strtokIndx = strtok(NULL, ",");
	if (strtokIndx == NULL) {
		return;
	}
	lastCommand.param8 = atoi(strtokIndx);     // convert this part to an integer
}

Command CommandParser::update()
{
	recvWithStartEndMarkers();
	if (newData == true) {
		strcpy(tempChars, receivedChars);
			// this temporary copy is necessary to protect the original data
			//   because strtok() used in parseData() replaces the commas with \0
		parseData();
		// showParsedData();
		newData = false;
	} else {
    lastCommand.isValid = false;
	}
  return lastCommand;
}
