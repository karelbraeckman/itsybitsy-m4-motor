/*
	Motor.h - Library for driving a motor using two PWM pins
	Created by Karel Braeckman, sep 2020
	Do whatever you want with it
*/

#ifndef Motor_h
#define Motor_h
#include "Arduino.h"

class Motor {
	public:
		Motor(int pin1, int pin2);
		void setPWM(int pwm1, int pwm2);
    void setPWMSpeed(int pwmSpeed);
		void forward();
		void backward();
	private:
		int _pin1;
		int _pin2;
};

#endif
