/*
  PidMotor.h - Library for controlling motor speed with a PID
  Created by Karel Braeckman, sep 2020
  Do whatever you want with it
*/

#ifndef Pid_Motor_h
#define Pid_Motor_h
#include "Arduino.h"
#include <PID_v1.h>
#include "Motor.h"

class PidMotor {
  public:
    PidMotor(int pin1, int pin2);
    void setPWM(int pwm1, int pwm2);
    void setPWMSpeed(int pwmSpeed);
    double pidSetpoint; 
    double pidInput;
    double pidOutput;
    PID pid;
    double updatePid(double actualSpeed);
    void plot();
    void stop();
    void setTunings(double Kp, double Ki, double Kd);
    void setSpeed(long mmPerSec);
  private:
    Motor motor;
    double Kp;
    double Ki;
    double Kd;
};

#endif
