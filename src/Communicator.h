#ifndef Communicator_h
#define Communicator_h
#include "Arduino.h"

class Communicator {
  public:
    virtual ~Communicator() = default;
    virtual void print(const char c[]) = 0;
    virtual void println(const char c[]) = 0;
    virtual void print(String) = 0;
    virtual void println(String) = 0;
    virtual int available(void) = 0;
    virtual int read(void) = 0;
};

#endif
