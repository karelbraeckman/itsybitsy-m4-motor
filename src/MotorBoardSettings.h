#ifndef MOTOR_BOARD_SETTINGS_H
#define MOTOR_BOARD_SETTINGS_H

struct MotorSettings {
    unsigned char pwmPin1;
    unsigned char pwmPin2;
    unsigned char encoderPin1;
    unsigned char encoderPin2;
    bool enabled;
};

struct MotorBoardSettings {
    MotorSettings motorA;
    MotorSettings motorB;
    MotorSettings motorX;
    MotorSettings motorY;
    // unsigned char pinMotorLeft2;
    // unsigned char pinMotorLeft1;
    // unsigned char pinMotorRight2;
    // unsigned char pinMotorRight1;

    // unsigned char pinEncoderLeft1;
    // unsigned char pinEncoderLeft2;
    // unsigned char pinEncoderRight1;
    // unsigned char pinEncoderRight2;
};

#endif