#include "ItsyBitsyMotorBoard.h"
#include "SerialCommunicator.h"

ItsyBitsyMotorBoard::ItsyBitsyMotorBoard(void) {
    settings.motorA.enabled = true;
    settings.motorB.enabled = true;
    settings.motorX.enabled = false;
    settings.motorY.enabled = false;
  // default values for settings
  #ifdef ESP32
    settings.motorA.pwmPin2 = 32; // L2
    settings.motorA.pwmPin1 = 4; // L1
    settings.motorA.encoderPin1 = 23; // D9 B_L
    settings.motorA.encoderPin2 = 19; // D10 A_L

    settings.motorB.pwmPin2 = 25; // R2
    settings.motorB.pwmPin1 = 27; // R1
    settings.motorB.encoderPin1 = 26; // D11 B_R
    settings.motorB.encoderPin2 = 18; // D12 A_R

    settings.motorX.pwmPin2 = 33;
    settings.motorX.pwmPin1 = 2;
    settings.motorX.encoderPin1 = 39;
    settings.motorX.encoderPin2 = 35;

    settings.motorY.pwmPin2 = 0;
    settings.motorY.pwmPin1 = 5;
    settings.motorY.encoderPin1 = 34;
    settings.motorY.encoderPin2 = 36;
  #else
    settings.motorA.pwmPin2 = MOSI; // L2
    settings.motorA.pwmPin1 = SCK; // L1 
    settings.motorA.encoderPin1 = 9; // D9 B_L
    settings.motorA.encoderPin2 = 10; // D10 A_L

    settings.motorB.pwmPin2 = A5; // R2
    settings.motorB.pwmPin1 = A4; // R1
    settings.motorB.encoderPin1 = 12; // D11 B_R
    settings.motorB.encoderPin2 = 11; // D12 A_R
  #endif
}

void ItsyBitsyMotorBoard::setup(long pulsesPerRev, int wheelDiameterMillimeters, int leftRightWheelDistanceMillimeters) {
    _pulsesPerRev = pulsesPerRev;
    _wheelDiameterMillimeters = wheelDiameterMillimeters;
    _leftRightWheelDistanceMillimeters = leftRightWheelDistanceMillimeters;
    _wheelCircumferenceMillimeter = ((float)wheelDiameterMillimeters) * PI;
    _robotCircumferenceMillimeter = ((float)leftRightWheelDistanceMillimeters) * PI;

    if (settings.motorA.enabled) {
      motorLeft = new PidMotor (settings.motorA.pwmPin1, settings.motorA.pwmPin2);
      encoderLeft = new Encoder (settings.motorA.encoderPin2, settings.motorA.encoderPin1);
      distanceDriverLeft = new DistanceDriver (motorLeft, _pulsesPerRev, _wheelCircumferenceMillimeter);
    } else {
      motorLeft = NULL;
      encoderLeft = NULL;
      distanceDriverLeft = NULL;
    }
    if (settings.motorB.enabled) {
      motorRight = new PidMotor (settings.motorB.pwmPin1, settings.motorB.pwmPin2);
      encoderRight = new Encoder(settings.motorB.encoderPin2, settings.motorB.encoderPin1);
      distanceDriverRight = new DistanceDriver (motorRight, _pulsesPerRev, _wheelCircumferenceMillimeter);
    } else {
      motorRight = NULL;
      encoderRight = NULL;
      distanceDriverRight = NULL;
    }
    if (settings.motorX.enabled) {
      motorX = new PidMotor (settings.motorX.pwmPin1, settings.motorX.pwmPin2);
      encoderX = new Encoder(settings.motorX.encoderPin2, settings.motorX.encoderPin1);
      distanceDriverX = new DistanceDriver (motorX, _pulsesPerRev, _wheelCircumferenceMillimeter);
    } else {
      motorX = NULL;
      encoderX = NULL;
      distanceDriverX = NULL;
    }
    if (settings.motorY.enabled) {
      motorY = new PidMotor (settings.motorY.pwmPin1, settings.motorY.pwmPin2);
      encoderY = new Encoder(settings.motorY.encoderPin2, settings.motorY.encoderPin1);
      distanceDriverY = new DistanceDriver (motorY, _pulsesPerRev, _wheelCircumferenceMillimeter);
    } else {
      motorY = NULL;
      encoderY = NULL;
      distanceDriverY = NULL;
    }
 
    // set up encoderSender, which outputs encoder values at most 25 times per second and prints them to Serial1 at most 1 times per second 
    encoderSender = new EncoderSender (encoderLeft, encoderRight, encoderX, encoderY, pulsesPerRev, _wheelCircumferenceMillimeter, _leftRightWheelDistanceMillimeters, 40, 30);

    kinematics = new Kinematics (MOTOR_MAX_RPM, (float)_wheelDiameterMillimeters/1000.0, (float)_leftRightWheelDistanceMillimeters/1000.0);

    waitForCommandName = "";
    waitForCommandId = -1;
    resolveWaitCommandOnMillis = -1;

    millisNow = 0;

    goToGoalEnabled = false;
    goal = {
        mmX: 0,
        mmY: 0,
        degreesPhi: 0
    };

    // use Serial1 by default
    communicator = new SerialCommunicator(&Serial1);
    setCommunicator(communicator);
    //setCommandSerial(&Serial1);

    //Serial.println("initialization done");
}

//void ItsyBitsyMotorBoard::setCommandSerial(Stream *streamObject) {
//  communicator->setSerial(streamObject);
  //commandSerial = streamObject;
  //commandParser.setInterface(streamObject);
  //encoderSender->setInterface(streamObject);
//}

void ItsyBitsyMotorBoard::setCommunicator(Communicator *theCommunicator) {
  communicator = theCommunicator;
  commandParser.setCommunicator(theCommunicator);
  encoderSender->setCommunicator(theCommunicator);
}

void ItsyBitsyMotorBoard::resolveCommandId(int commandId) {
  // print out OK with command id
  String output;
  output = "<OK,";
  output += commandId;
  output += ">\n";
  communicator->print(output);
  //commandSerial->print("<OK,");
  //commandSerial->print(commandId);
  //commandSerial->print(">\n");
  if (commandId == waitForCommandId) {
    waitForCommandId = -1;
    waitForCommandName = "";
  }
}

void ItsyBitsyMotorBoard::rejectCommandId(int commandId, String message) {
  String output;
  output = "<NOK,";
  output += commandId;
  output += ",";
  
  //commandSerial->print("<NOK,");
  //commandSerial->print(commandId);
  //commandSerial->print(",");

  String escapedMessage = message;
  escapedMessage.replace(",", ";");
  escapedMessage.replace("<", "(");
  escapedMessage.replace(">", ")");
  output += escapedMessage;
  //commandSerial->print(escapedMessage);
  //commandSerial->print(">\n");
  output += ">\n";
  communicator->print(output);
  
  if (commandId == waitForCommandId) {
    waitForCommandId = -1;
    waitForCommandName = "";
  }
}

void ItsyBitsyMotorBoard::waitForCommand(int commandId, String commandName) {
  waitForCommandName = commandName;
  waitForCommandId = commandId;
}

/**
 * Set speed using linear & angular speeds
 * vLinear in mm/sec
 * omega in deg/s
 */
void ItsyBitsyMotorBoard::setSpeed2(long vLinear, long omega) {
  float linear_vel_x = (double)vLinear/1000.0;  // param1 in mm per sec
  float linear_vel_y = 0;  // 0 m/s
  float angular_vel_z = (double)omega * PI / 180.0; // param2 in deg/s
  //given the required velocities for the robot, you can calculate the rpm required for each motor
  Kinematics::output rpm = kinematics->getRPM(linear_vel_x, linear_vel_y, angular_vel_z);
  long leftMotorSpeedMmSec = rpm.motor1 * _wheelCircumferenceMillimeter / 60.0;
  long rightMotorSpeedMmSec = rpm.motor2 * _wheelCircumferenceMillimeter / 60.0;

  if (motorLeft != NULL) {
    motorLeft->setSpeed((long)leftMotorSpeedMmSec);
  }
  if (motorRight != NULL) {
    motorRight->setSpeed((long)rightMotorSpeedMmSec);
  }
}


bool ItsyBitsyMotorBoard::handleCommand(Command *theCommand) {
  if (!theCommand->isValid) {
    return false;
  }
  if (strcmp(theCommand->name, "STOP") != 0) {
    Serial.print("GOT Command ");
    Serial.print(theCommand->id);
    Serial.print(": ");
    Serial.println(theCommand->name);
  }

  if (strcmp(theCommand->name, "PWM_SET") == 0) {
//    Serial.println("Executing setPWM command!");
    if (motorLeft != NULL) {
      motorLeft->setPWM(theCommand->param1, theCommand->param2);
    }
    if (motorRight != NULL) {
      motorRight->setPWM(theCommand->param3, theCommand->param4);
    }
    if (motorX != NULL) {
      motorX->setPWM(theCommand->param5, theCommand->param6);
    }
    if (motorY != NULL) {
      motorY->setPWM(theCommand->param7, theCommand->param8);
    }
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "PWM_SET_A") == 0) {
    if (motorLeft != NULL) {
      motorLeft->setPWM(theCommand->param1, theCommand->param2);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "PWM_SET_B") == 0) {
    if (motorRight != NULL) {
      motorRight->setPWM(theCommand->param1, theCommand->param2);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "PWM_SET_X") == 0) {
    if (motorX != NULL) {
      motorX->setPWM(theCommand->param1, theCommand->param2);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "PWM_SET_Y") == 0) {
    if (motorY != NULL) {
      motorY->setPWM(theCommand->param1, theCommand->param2);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "STOP") == 0) {
    if (motorLeft != NULL) {
      motorLeft->stop();
    }
    if (motorRight != NULL) {
      motorRight->stop();
    }
    if (motorX != NULL) {
      motorX->stop();
    }
    if (motorY != NULL) {
      motorY->stop();
    }
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "SPEED_SET") == 0) {
    if (motorLeft != NULL) {
      motorLeft->setSpeed((long)theCommand->param1);
    }
    if (motorRight != NULL) {
      motorRight->setSpeed((long)theCommand->param2);
    }
    long duration = (long)theCommand->param3;
    if (motorX != NULL) {
      motorX->setSpeed((long)theCommand->param3);
      duration = (long)theCommand->param4;
    }
    if (motorY != NULL) {
      motorY->setSpeed((long)theCommand->param4);
      duration = (long)theCommand->param5;
    }
    
    if (duration > 0) {
      // wait duration, then stop
      resolveWaitCommandOnMillis = millis() + duration;
      waitForCommand(theCommand->id, theCommand->name);
    } else {
      resolveCommandId(theCommand->id);
    }
    return true;
  } 
  if (strcmp(theCommand->name, "SPEED_SET_A") == 0) {
    if (motorLeft != NULL) {
      motorLeft->setSpeed((long)theCommand->param1);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    long duration = (long)theCommand->param2;
    if (duration > 0) {
      // wait duration, then stop
      resolveWaitCommandOnMillis = millis() + duration;
      waitForCommand(theCommand->id, theCommand->name);
    } else {
      resolveCommandId(theCommand->id);
    }
    return true;
  } 
  if (strcmp(theCommand->name, "SPEED_SET_B") == 0) {
    if (motorRight != NULL) {
      motorRight->setSpeed((long)theCommand->param1);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    long duration = (long)theCommand->param2;
    if (duration > 0) {
      // wait duration, then stop
      resolveWaitCommandOnMillis = millis() + duration;
      waitForCommand(theCommand->id, theCommand->name);
    } else {
      resolveCommandId(theCommand->id);
    }
    return true;
  } 
  if (strcmp(theCommand->name, "SPEED_SET_X") == 0) {
    if (motorX != NULL) {
      motorX->setSpeed((long)theCommand->param1);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    long duration = (long)theCommand->param2;
    if (duration > 0) {
      // wait duration, then stop
      resolveWaitCommandOnMillis = millis() + duration;
      waitForCommand(theCommand->id, theCommand->name);
    } else {
      resolveCommandId(theCommand->id);
    }
    return true;
  } 
  if (strcmp(theCommand->name, "SPEED_SET_Y") == 0) {
    if (motorY != NULL) {
      motorY->setSpeed((long)theCommand->param1);
    } else {
      rejectCommandId(theCommand->id, "NOT_ENABLED");
      return true;
    }
    
    long duration = (long)theCommand->param2;
    if (duration > 0) {
      // wait duration, then stop
      resolveWaitCommandOnMillis = millis() + duration;
      waitForCommand(theCommand->id, theCommand->name);
    } else {
      resolveCommandId(theCommand->id);
    }
    return true;
  } 
  if (strcmp(theCommand->name, "SPEED_SET2") == 0) {
    long vLinear = (long)theCommand->param1; // mm/s
    long omega = (long)theCommand->param2; // deg/s
    setSpeed2(vLinear, omega);
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "DRIVE_DIST") == 0) {
    Serial.println("drive dist");
    Serial.println((long)theCommand->param1);
    Serial.println((long)theCommand->param2);
    if (distanceDriverLeft != NULL) {
      distanceDriverLeft->driveMillimeters((long)theCommand->param1);
    }
    if (distanceDriverRight != NULL) {
      distanceDriverRight->driveMillimeters((long)theCommand->param2);
    }
    if (distanceDriverX != NULL) {
      distanceDriverX->driveMillimeters((long)theCommand->param3);
    }
    if (distanceDriverY != NULL) {
      distanceDriverY->driveMillimeters((long)theCommand->param4);
    }
    waitForCommand(theCommand->id, theCommand->name);
    return true;
  } 
  if (strcmp(theCommand->name, "TURN") == 0) {
    // TODO: this turns at a constant speed which is not always necessary. Another implementation
    // might use a PID to reach the target angle as soon as possible
    float angle = (float)theCommand->param1; // in degrees
    long distance = (long)(_robotCircumferenceMillimeter * angle / 360.0);
    if (distanceDriverLeft != NULL) {
      distanceDriverLeft->driveMillimeters(-distance);
    }
    if (distanceDriverRight != NULL) {
      distanceDriverRight->driveMillimeters(distance);
    }
    waitForCommand(theCommand->id, theCommand->name);
    return true;
  } 
  if (strcmp(theCommand->name, "PID_TUNE") == 0) {
    double Kp = ((double)theCommand->param1)/100.0;
    double Ki = ((double)theCommand->param2)/100.0;
    double Kd = ((double)theCommand->param3)/100.0;
    //commandSerial->print("setTunings Kp=");
    //commandSerial->print(Kp,2);
    //commandSerial->print(";Ki=");
    //commandSerial->print(Ki,2);
    //commandSerial->print(";Kd=");
    //commandSerial->print(Kd,2);
    //commandSerial->print("\n");
    if (motorLeft != NULL) {
      motorLeft->setTunings(Kp, Ki, Kd);
    }
    if (motorRight != NULL) {
      motorRight->setTunings(Kp, Ki, Kd);
    }
    if (motorX != NULL) {
      motorX->setTunings(Kp, Ki, Kd);
    }
    if (motorY != NULL) {
      motorY->setTunings(Kp, Ki, Kd);
    }
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "SETUP") == 0) {
    if (theCommand->param1 > 0) {
      _leftRightWheelDistanceMillimeters = theCommand->param1;
    }
    if (theCommand->param2 > 0) {
      _pulsesPerRev = (long)theCommand->param2;
    }
    if (theCommand->param3 > 0) {
      _wheelDiameterMillimeters = theCommand->param3;
    }
    _wheelCircumferenceMillimeter = ((float)_wheelDiameterMillimeters) * PI;
    _robotCircumferenceMillimeter = ((float)_leftRightWheelDistanceMillimeters) * PI; 
    if (distanceDriverLeft != NULL) {
      distanceDriverLeft->setup(_pulsesPerRev, _wheelCircumferenceMillimeter);
    }
    if (distanceDriverRight != NULL) {
      distanceDriverRight->setup(_pulsesPerRev, _wheelCircumferenceMillimeter);
    }
    if (distanceDriverX != NULL) {
      distanceDriverX->setup(_pulsesPerRev, _wheelCircumferenceMillimeter);
    }
    if (distanceDriverY != NULL) {
      distanceDriverY->setup(_pulsesPerRev, _wheelCircumferenceMillimeter);
    }
    encoderSender->setup(_pulsesPerRev, _wheelCircumferenceMillimeter, _leftRightWheelDistanceMillimeters);
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "SET_POSE") == 0) {
    Pose newPose = {
      mmX: theCommand->param1,
      mmY: theCommand->param2,
      degreesPhi: theCommand->param3
    };
    encoderSender->setPose(&newPose);   
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "SET_GOAL") == 0) {
    goal.mmX = theCommand->param1;
    goal.mmY = theCommand->param2;
    goal.degreesPhi = theCommand->param3;
    goToGoalEnabled = true;
    resolveCommandId(theCommand->id);
    return true;
  } 
  if (strcmp(theCommand->name, "CLEAR_GOAL") == 0) {
    goToGoalEnabled = false;
    return true;
  }  
  return false; // inform calling environment the command could not be handled by us
}

void ItsyBitsyMotorBoard::loop(void) {
    millisNow = millis();

    if (resolveWaitCommandOnMillis > 0 && millisNow >= resolveWaitCommandOnMillis) {
        resolveWaitCommandOnMillis = -1;
        if (motorLeft != NULL) {
          motorLeft->stop();
        }
        if (motorRight != NULL) {
          motorRight->stop();
        }
        if (motorX != NULL) {
          motorX->stop();
        }
        if (motorY != NULL) {
          motorY->stop();
        }
        resolveCommandId(waitForCommandId);
    }
    
    encoderUpdate = encoderSender->update();
    if (encoderUpdate->period > 0) { // period positive when update happened   
        mmSpeedLeft = encoderUpdate->mmSpeedLeft;
        mmSpeedRight = encoderUpdate->mmSpeedRight;
        mmSpeedX = encoderUpdate->mmSpeedX;
        mmSpeedY = encoderUpdate->mmSpeedY;

        if (motorLeft != NULL) {
          motorLeft->updatePid((double)mmSpeedLeft);
        }
        if (motorRight != NULL) {
          motorRight->updatePid((double)mmSpeedRight);
        }
        if (motorX != NULL) {
          motorX->updatePid((double)mmSpeedX);
        }
        if (motorY != NULL) {
          motorY->updatePid((double)mmSpeedY);
        }

        // pass encoder info to distanceDriver
        if (distanceDriverLeft != NULL) {
          distanceDriverLeft->gotTicks(encoderUpdate->ticksLeft);
        }
        if (distanceDriverRight != NULL) {
          distanceDriverRight->gotTicks(encoderUpdate->ticksRight);
        }
        if (distanceDriverX != NULL) {
          distanceDriverX->gotTicks(encoderUpdate->ticksX);
        }
        if (distanceDriverY != NULL) {
          distanceDriverY->gotTicks(encoderUpdate->ticksY);
        }
        
        if (waitForCommandId !=-1 && (waitForCommandName == "DRIVE_DIST" || waitForCommandName == "TURN")) {
          if (
            (distanceDriverLeft == NULL || !distanceDriverLeft->isBusy())
            && (distanceDriverRight == NULL || !distanceDriverRight->isBusy())
            && (distanceDriverX == NULL || !distanceDriverX->isBusy())
            && (distanceDriverY == NULL || !distanceDriverY->isBusy())
          ) {
            resolveCommandId(waitForCommandId);
          }
        }
        currentPose = encoderSender->getPose();
        // pass location estimate to go-to-goal behaviour
        if (goToGoalEnabled) {
            double distanceToGoal = sqrt(pow(goal.mmY - currentPose->mmY, 2) + pow(goal.mmX - currentPose->mmX, 2));
            if (distanceToGoal > 20) {
                // desired heading, in radians, between [-PI..PI]
                double phiDesiredRadians = atan2((double)goal.mmY - (double)currentPose->mmY, (double)goal.mmX - (double)currentPose->mmX);
                long phiDesiredDegrees = phiDesiredRadians * 180.0 / PI;
                double phiCurrentDegrees = currentPose->degreesPhi;
                double headingError = (phiDesiredDegrees - phiCurrentDegrees);
                while (headingError > 180) {
                    headingError -= 360;
                }
                while (headingError < -180) {
                    headingError += 360;
                }
                double KpGoToGoal = 1.0;
                double omega = KpGoToGoal * headingError;
                long v0 = 80;
                setSpeed2(v0, omega);
            } else {
                if (motorLeft != NULL) {
                  motorLeft->stop();
                }
                if (motorRight != NULL) {
                  motorRight->stop();
                }
                if (motorX != NULL) {
                  motorX->stop();
                }
                if (motorY != NULL) {
                  motorY->stop();
                }
                goToGoalEnabled = false;
            }
        }
    }

    //Command incomingCommand = commandParser.update();
    //handleCommand(&incomingCommand);
}

