#ifndef SerialCommunicator_h
#define SerialCommunicator_h

#include "Communicator.h"

class SerialCommunicator : public Communicator {
  public:
    SerialCommunicator();
    SerialCommunicator(Stream *streamObject);
    void print(const char c[]);
    void println(const char c[]);
    void print(String);
    void println(String);
    void setSerial(Stream *streamObject);
    Stream *commandSerial;
    int available(void);
    int read(void);
};

#endif