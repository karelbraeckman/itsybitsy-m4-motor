#include <ItsyBitsyMotorBoard.h>

ItsyBitsyMotorBoard motorBoard;

void setup() {
	Serial.begin(115200); // usb connection for debugging
	Serial1.begin(115200); // interface to other microcontroller / single-board computer
	
  // gear ratio of 50:1 and 12 encoder ticks per ungeared revolution
  long gearRatio = 50L;
  long pulsesPerRev = gearRatio * 12L;
  int wheelDiameterMillimeters = 90;
  int leftRightWheelDistanceMillimeters = 160;
  motorBoard.setup(pulsesPerRev, wheelDiameterMillimeters, leftRightWheelDistanceMillimeters);
}

bool handleCommandCustom(Command *theCommand) {
	if (!theCommand->isValid) {
		return false;
	}
	
	// example of handling a custom command you define
	if (strcmp(theCommand->name, "SAY_HI") == 0) {
		Serial.println("Hello world");
		motorBoard.resolveCommandId(theCommand->id);
		return true;
	}
	
	// commands not handled by board or custom commands
	Serial.print("Unknown command \"");
	Serial.print(theCommand->name);
	Serial.print("\"\n");
	String temp = "Unknown Command \"";
	temp += theCommand->name;
	temp += "\"";
	motorBoard.rejectCommandId(theCommand->id, temp);
  
  return false;
}

void loop() {
	motorBoard.loop();
	Command incomingCommand = motorBoard.commandParser.update();
	if (!incomingCommand.isValid) {
		return;
	}
	bool handled = motorBoard.handleCommand(&incomingCommand);
	Serial.print("Handled by board lib: ");
	Serial.println(handled);
	if (!handled) {
	  handled = handleCommandCustom(&incomingCommand);
	  Serial.print("Handled by custom: ");
	  Serial.println(handled);
	}
}